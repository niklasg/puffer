import Vue from 'vue'
import Router from 'vue-router'
import TheFeed from '@/components/TheFeed'
import NewPost from '@/components/NewPost'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Feed',
      component: TheFeed
    }, {
      path: '/post',
      name: 'NewPost',
      component: NewPost
    }
  ]
})
