import axios from 'axios'

export default {
  state: {
    posts: [],
    self: {
      name: 'John Doe',
      id: 'john_d',
      image: 'static/img/profile-pictures/john.jpg'
    }
  },

  mutations: {
    ADD_POST (state, post) {
      state.posts.unshift(post)
    },

    SET_POSTS (state, posts) {
      state.posts = posts
    },

    REMOVE_POST (state, post) {
      const index = state.posts.indexOf(post)

      state.posts.splice(index, 1)
    }
  },

  actions: {
    async loadPosts ({ commit }) {
      const { data } = await axios.get('http://localhost:3000/api/posts?_sort=time&_order=desc')

      commit('SET_POSTS', data)
    },

    async savePost ({ commit, state }, post) {
      const postWithTempID = { id: state.posts.length * -1, ...post }

      commit('ADD_POST', postWithTempID)

      axios.post('http://localhost:3000/api/posts', post).catch(() => {
        commit('REMOVE_POST', postWithTempID)
      })
    }
  },

  getters: {
    posts (state) {
      return state.posts.map(post => ({
        postedByMe: post.user.id === state.self.id,
        ...post
      }))
    },

    self (state) {
      return state.self
    }
  }
}
