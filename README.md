# Puffer

A fake social media PWA client. It's a playground for load time optimization strategies. Each commit represents a stage of optimization. Checkout individual commits to see how performance increasingly improves.

To run it locally, clone this repository. Open a terminal and navigate to its root directory. Build files are included so dependencies don't need to be installed. Make sure `yarn` or `npm` is installed on your machine, then run:

```sh
yarn run serve
# or
npm run serve
```

to create a local web server. The PWA should now be accessable at [http://localhoast:8080](http://localhoast:8080/).

Puffer also relies on a simple REST API on port 3000. Run:

```sh
yarn run api
# or
npm run api
```

in a separate process to start it too.
